﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CameraStatus
{
    static_Camera = 0,
    lerp_Camera,
    jump_Camera
}

public enum Movement
{
    instant = 0,
    lerp
}

public enum AbsOrRel
{
    relative = 0,
    absolute
}

public class RoomSwitchCamera : MonoBehaviour
{

    [SerializeField] private GameObject[] m_Colliders;
    [SerializeField] private Vector2 m_Screensize;

    [SerializeField] private Player_Script m_Player;

    [SerializeField] private float m_CamerLerpTime;
    [SerializeField] private Vector3 m_CameraStartPosition;
    [SerializeField] private Vector3 m_CameraForm;
    [SerializeField] private Vector3 m_CameraTo;
    private float m_CurrentLerpTime = 0f;

    [SerializeField] private CameraStatus m_CameraStatus = CameraStatus.static_Camera;


    public void ResetCameraLerp()
    {
        m_CurrentLerpTime = 0f;
    }

    private void CompletedCameraMovement()
    {
        m_CameraStatus = CameraStatus.static_Camera;

        if (m_Player != null)
        {
            m_Player.PausePlayer(false);
        }
    }

    private void Start()
    {
        m_Player = FindObjectOfType<Player_Script>();

        if(m_Player == null)
        {
            Debug.LogError("Player not found in " + name);
        }

        m_CameraStartPosition = transform.position;

        CalculateScreenSize();
        CreateColliders();
        PositionColliders();
    }

    private void CalculateScreenSize()
    {
        m_Screensize = new Vector2();

        m_Screensize.x = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0)));
        m_Screensize.y = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(Screen.height, 0)));
    }

    private void CreateColliders()
    {
        m_Colliders = new GameObject[4];

        for (int i = 0; i < m_Colliders.Length; i++)
        {
            m_Colliders[i] = new GameObject();

            BoxCollider2D col = m_Colliders[i].AddComponent<BoxCollider2D>();
            col.isTrigger = true;

            m_Colliders[i].transform.parent = transform;

            RoomSwitchCameraTrigger trigger = m_Colliders[i].AddComponent<RoomSwitchCameraTrigger>();

            trigger.SetParent(this);
        }

        m_Colliders[0].name = "Top Collider";
        m_Colliders[1].name = "Right Collider";
        m_Colliders[2].name = "Bottom Collider";
        m_Colliders[3].name = "Left Collider";
    }

    public void MoveCamera(int x, int y, AbsOrRel absrel = AbsOrRel.relative, Movement how = Movement.lerp)
    {
        if(absrel == AbsOrRel.relative)
        {
            m_CameraForm = transform.position;
        }
        else if(absrel == AbsOrRel.absolute)
        {
            m_CameraForm = m_CameraStartPosition;
        }

        m_CameraTo = m_CameraForm + new Vector3(x * m_Screensize.x, y * m_Screensize.y);

        ResetCameraLerp();

        if (how == Movement.lerp)
        {
            m_CameraStatus = CameraStatus.lerp_Camera;
        }
        else if (how == Movement.instant)
        {
            m_CameraStatus = CameraStatus.jump_Camera;
        }

        if (m_Player != null)
        {
            m_Player.PausePlayer(true);
        }
    }

    private void ProcessCameraMovement()
    {
        if (m_CameraStatus == CameraStatus.jump_Camera)
        {
            transform.position = m_CameraTo;
            CompletedCameraMovement();
            return;
        }

        if (m_CameraStatus == CameraStatus.lerp_Camera)
        {
            m_CurrentLerpTime += Time.deltaTime;

            if (m_CurrentLerpTime > m_CamerLerpTime)
            {
                m_CurrentLerpTime = m_CamerLerpTime;
            }

            float percentage = m_CurrentLerpTime / m_CamerLerpTime;

            transform.position = Vector3.Lerp(m_CameraForm, m_CameraTo, percentage);

            if (percentage.Equals(1f))
            {
                CompletedCameraMovement();
            }
        }
    }

    private void PositionColliders()
    {
        float colDepth = 0.5f;
        float zDepth = 1f;
        float offsetfromedge = 0.5f;

        CalculateScreenSize();

        //Top
        m_Colliders[0].transform.localScale = new Vector3(m_Screensize.x, colDepth, colDepth);
        m_Colliders[0].transform.position = new Vector3(transform.position.x, transform.position.y + (m_Screensize.y * 0.5f) + (m_Colliders[0].transform.localScale.y * 0.5f) + offsetfromedge, zDepth);

        //Right
        m_Colliders[1].transform.localScale = new Vector3(colDepth, m_Screensize.y, colDepth);
        m_Colliders[1].transform.position = new Vector3(transform.position.x + (m_Screensize.x * 0.5f) + (m_Colliders[1].transform.localScale.x * 0.5f) + offsetfromedge, transform.position.y, zDepth);

        //Bottom
        m_Colliders[2].transform.localScale = new Vector3(m_Screensize.x, colDepth, colDepth);
        m_Colliders[2].transform.position = new Vector3(transform.position.x , transform.position.y - (m_Screensize.y * 0.5f) - (m_Colliders[2].transform.localScale.y * 0.5f) - offsetfromedge, zDepth);

        //Left
        m_Colliders[3].transform.localScale = new Vector3(colDepth, m_Screensize.y, colDepth);
        m_Colliders[3].transform.position = new Vector3(transform.position.x - (m_Screensize.x * 0.5f) - (m_Colliders[3].transform.localScale.x * 0.5f) - offsetfromedge, transform.position.y, zDepth);
    }

    private void LateUpdate()
    {
        PositionColliders();

        if( m_CameraStatus != CameraStatus.static_Camera)
        {
            ProcessCameraMovement();
        }
    }

    public void ProcessTriggerEnter2D(Collider2D other, string collidername)
    {
        if(other.gameObject == m_Player.gameObject)
        {
            if(collidername == "Top Collider")
            {
                MoveCamera(0,1);
            }
            if (collidername == "Right Collider")
            {
                MoveCamera(1,0);
            }
            if (collidername == "Bottom Collider")
            {
                MoveCamera(0, -1);
            }
            if (collidername == "Left Collider")
            {
                MoveCamera(-1, 0);
            }
        }
    }
}
