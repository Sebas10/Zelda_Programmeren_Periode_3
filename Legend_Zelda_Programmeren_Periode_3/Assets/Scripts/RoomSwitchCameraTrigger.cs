﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomSwitchCameraTrigger : MonoBehaviour {

    private RoomSwitchCamera m_RoomSwitchCamera;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetParent(RoomSwitchCamera cam)
    {
        m_RoomSwitchCamera = cam;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (m_RoomSwitchCamera != null)
        {
            m_RoomSwitchCamera.ProcessTriggerEnter2D(other, name);
        }
    }
}
