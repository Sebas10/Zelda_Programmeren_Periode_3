﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState
{
    idle = 0,
    walking,
    attack_sword,
    attack_wand,
    attack_other,
    cheer,
    death
}
public enum Direction
{
    up = 0,
    right,
    down,
    left
}

public class Player_Script : MonoBehaviour
{

    [SerializeField]
    private Vector3 Speed;

    [SerializeField] private float MaxSpeedTilesSec, xAxis, yAxis;

    private Animator m_Anim;

    private bool fire1, fire2, fire3, attack;

    private Direction direction;
    private PlayerState playerState;

    private bool pause = false;

    // Use this for initialization
    void Start()
    {
        m_Anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (pause)
        {
            return;
        }

        xAxis = Input.GetAxis("Horizontal");
        yAxis = Input.GetAxis("Vertical");

        fire1 = Input.GetButton("Fire1");
        fire2 = Input.GetButton("Fire2");
        fire3 = Input.GetButton("Fire3");
        attack = Input.GetButton("Attack");

        AnimDir();

        Speed = new Vector3(xAxis * MaxSpeedTilesSec, yAxis * MaxSpeedTilesSec);

        transform.position += Speed * Time.deltaTime;

        m_Anim.SetFloat("Direction", (float)direction);
        m_Anim.SetInteger("State", (int)playerState);

        if (xAxis == 0 && yAxis == 0)
            playerState = PlayerState.idle;
        else
            playerState = PlayerState.walking;

        if (attack)
            playerState = PlayerState.attack_sword;
        else if (fire1)
            playerState = PlayerState.attack_wand;
        else if (fire2)
            playerState = PlayerState.attack_other;
        else if (fire3)
            playerState = PlayerState.death;
    }

    private void AnimDir()
    {
        if (xAxis < 0)
        {
            direction = Direction.left;
            yAxis = 0;
        }
        else if (xAxis > 0)
        {
            direction = Direction.right;
            yAxis = 0;
        }

        if (yAxis < 0)
        {
            direction = Direction.down;
            xAxis = 0;
        }
        else if (yAxis > 0)
        {
            direction = Direction.up;
            xAxis = 0;
        }

    }

    public void PausePlayer(bool b)
    {
        pause = b;
    }
}
